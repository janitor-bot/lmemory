#include "default.xpm"

     /* example-start buttons buttons.c */

     #include <gtk/gtk.h>

     GtkWidget *xpm_widget( GtkWidget *parent,
                               gchar     *xpm_filename)
     {
         GtkWidget *pixmapwid;
         GdkPixmap *pixmap;
         GdkBitmap *mask;
         GtkStyle *style;

         /* Get the style of the button to get the
          * background color. */
         style = gtk_widget_get_style(parent);

         /* Now on to the xpm stuff */
         pixmap = gdk_pixmap_create_from_xpm (parent->window, &mask,
                                              &style->bg[GTK_STATE_NORMAL],
                                              xpm_filename);
         pixmapwid = gtk_pixmap_new (pixmap, mask);

         return(pixmapwid);
     }



     /* example-start pixmap pixmap.c */


     GtkWidget *xpm_widget_default( GtkWidget *parent)
     {
         /* GtkWidget is the storage type for widgets */
         GtkWidget *pixmapwid;
         GdkPixmap *pixmap;
         GdkBitmap *mask;
         GtkStyle *style;

         /* now for the pixmap from gdk */
         style = gtk_widget_get_style( parent );
         pixmap = gdk_pixmap_create_from_xpm_d( parent->window,  &mask,
                                                &style->bg[GTK_STATE_NORMAL],
                                                (gchar **)default_xpm );

         /* a pixmap widget to contain the pixmap */
         pixmapwid = gtk_pixmap_new( pixmap, mask );

         return(pixmapwid);
     }
