This should be shown in a window when "Info" button is pressed.

The aim of the game is to remove the cards in pairs by matching
the picture on the cards. There are five skill levels that can
be changed by pressing the skill-level button which displays one
of "Little One", "Beginner", "Skilled", "Master", and "Daemon".

Description of the skill levels:

Little One:
All cards are turned up. A card is flipped after it is clicked.
When two matching cards are clicked they are removed.

Beginner:
The same as the classical card game "Memory". Everything is static.

Skilled:
Same as "Beginner" except each time a pair of cards are removed
all remaining cards are moved to the left by one space. Their
relative positions remain the same, except the first card now
becomes the last card. This is not much harder than the classical
game.

Master:
At this level, all cards are moved to the left by one space whenever
two cards are flipped, whether or not they are matched and removed.
This is already very challenging for myself.

Daemon:
Now all cards are moved to the left by one space when two cards are
flipped but not matched. When two cards are matched and removed then
the remaining cards are moved by a random number of spaces, keeping
their relative positions fixed. This level is for people with the
best brains.
